package es.almata.aportal.refaccio;

public class Persones {
	
	String nom;
	String cognoms;
	int edat;

	
	public Persones(String nom, String cognoms, int edat) {
		super();
		this.nom = nom;
		this.cognoms = cognoms;
		this.edat = edat;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getCognoms() {
		return cognoms;
	}
	public void setCognoms(String cognoms) {
		this.cognoms = cognoms;
	}
	public int getEdat() {
		return edat;
	}
	public void setEdat(int edat) {
		this.edat = edat;
	}
	
	
}
