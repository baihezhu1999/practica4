package Test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;

import es.almata.calculadora.Calculadora;


class TestCalculadora {
	
	@ParameterizedTest
	@CsvSource({"12,4,16",
				"22,1,23",
				"23,2,25",
				"24,3,27",
				"33,22,55"})
	@DisplayName("An+Bn=Cn")
	void addParamArray(int a,int b,int resultat) {
		Calculadora c = new Calculadora();
		assertEquals(resultat, c.add(a, b));
	}
	@ParameterizedTest
	@CsvSource({"12,4,8",
				"22,1,21",
				"23,2,21",
				"24,3,21",
				"33,22,11"})
	@DisplayName("An-Bn=Cn")
	void subParamArray(int a,int b,int resultat) {
		Calculadora c = new Calculadora();
		assertEquals(resultat, c.sub(a, b));
	}
	@ParameterizedTest
	@CsvSource({"12,4,3",
				"22,1,22",
				"24,2,12",
				"27,3,9",
				"33,11,3"})
	@DisplayName("An/Bn=Cn")
	void divParamArray(int a,int b,int resultat) {
		Calculadora c = new Calculadora();
		assertEquals(resultat, c.div(a, b));
	}
	@ParameterizedTest
	@CsvSource({"12,4,48",
				"22,1,22",
				"24,2,48",
				"27,3,81",
				"33,22,726"})
	@DisplayName("An*Bn=Cn")
	void mulParamArray(int a,int b,int resultat) {
		Calculadora c = new Calculadora();
		assertEquals(resultat, c.mul(a, b));
	}
	@Disabled("desactivat")
	@ParameterizedTest
	@CsvSource({"hola,4",
				"superman,8",
				"astronauta,10"})
	@DisplayName("la paraula {0} hauria de tenir {1} caràcter")
	void addParamArray(String paraula,int longitud) {
		
		assertEquals(longitud,paraula.length());
	}
	
	
	@BeforeEach
	public void setUp() {
		System.out.println("s'executa a l'inici a cada test");
	}
	@AfterEach
	public void after() {
		System.out.println("s'executa a final a cada test");
	}
	@AfterAll
	public static void afterall() {
		System.out.println("despres de tots els tests");
	}
	@BeforeAll
	public static void beforeall() {
		System.out.println("abans de tots els tests");
	}
	
	
}

